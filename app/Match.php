<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Match extends Model {
    protected $fillable = [
        "hall_id",
        "name_home_team",
        "name_guest_team",
        "datetime"
    ];

    /**
     * Get the hall of the match
     */

    public function hall() {
        return $this->belongsTo("App\Hall");
    }
}
