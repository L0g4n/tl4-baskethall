<div class="modal fade" id="deleteHallModal{{ $loop->iteration }}" tabindex="-1" role="dialog" aria-labelledby="deleteHallModalLabel{{ $loop->iteration }}"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="deleteHallModalLabel{{ $loop->iteration }}">Löschen bestätigen</h5>

                <button type="button" class="close" data-dismiss="modal" aria-label="Abbrechen">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <p>Möchtest du die Halle <i>{{ $hall->name }}</i> wirklich löschen?<br/>
                <strong>Das kann nicht rückgängig gemacht werden!</strong></p>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Abbrechen</button>

                <form method="POST" action="{!! route("deleteHall", ["id" => $hall->id]) !!}">
                    {{ method_field("DELETE") }}
                    {{ csrf_field() }}
                    <button type="submit" class="btn btn-danger">Löschen</button>
                </form>
            </div>
        </div>
    </div>
</div>
