<div class="container-fluid p-3 ml-0">
<h2>Alle Partien</h2>

<div class="accordion" id="accordionMatches">
  <div class="card rounded">
    @foreach ($halls as $hall)
      @if (!count($hall->matches))
        @continue
      @endif

      <div class="card-header" id="heading{{ $loop->iteration }}">
        <h5 class="mb-0">
          <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapse{{ $loop->iteration }}" aria-expanded="true" aria-controls="collapse{{ $loop->iteration }}">
            {{ $hall->name }} <span class="badge badge-secondary">{{ count($hall->matches) }}</span>
          </button>
        </h5>
      </div>

      <div id="collapse{{ $loop->iteration }}" class="collapse" aria-labelledby="heading{{ $loop->iteration }}" data-parent="#accordionMatches">
        <div class="card-body">
          <div class="row">
            @foreach ($hall->matches->sortBy("datetime") as $match)
              <div class="col-sm-6 mb-4">
                <div class="card rounded">
                  <div class="card-body">
                    <dl>
                      <dt>Heimteam</dt><dd>{{ $match->name_home_team }}</dd>
                      <dt>Gastteam</dt><dd>{{ $match->name_guest_team }}</dd>
                      <dt>Datum</dt><dd>{{ \Carbon\Carbon::parse($match->datetime)->format("d.m.Y H:i") }}</dd>
                    </dl>
                    <button type="button" class="btn btn-outline-danger float-sm-right" data-toggle="modal" data-target="#deleteMatchModal{{ $loop->parent->iteration }}{{ $loop->iteration }}">Löschen</button>
                    @include("deleteMatchModal")
                  </div>
                </div>
              </div>
            @endforeach
          </div>
        </div>
      </div>
    @endforeach
  </div>
</div>
</div>
