@extends("main")

@section("content")

<div class="container-fluid p-3 ml-0">
<h2>Alle Hallen <span class="badge badge-secondary">{{ count($halls) }}</span></h2>
  <div class="row">
    @foreach($halls as $hall)
      <div class="col-sm-4 mb-4">
        <div class="card">
          <div class="card-body">
            <h5 class="card-title">{{ $hall->name }}</h5>
              <dl>
                <dt>Adresse</dt><dd>{{ $hall->address }}</dd>
                <dt>Platzanzahl</dt><dd>{{ $hall->seat_count }}</dd>
                <dt>Anzahl Ein-/Ausgänge</dt><dd>{{ $hall->entrance_exit_count }}</dd>
              </dl>
              <a href="{!! route("editHall", [$hall->id]) !!}" class="btn btn-outline-primary float-sm-left" role="button">Editieren</a>
              
              {{-- make deletion of halls only possible if it has no dedicates matches --}}
              @if (!count($hall->matches))
                <button type="button" class="btn btn-outline-danger float-sm-right" data-toggle="modal" data-target="#deleteHallModal{{ $loop->iteration }}">Löschen</button>

                @include("deleteHallModal")

                @else
                  <button type="button" class="btn btn-outline-danger float-sm-right" data-toggle="tooltip" data-placement="top" title="Partien noch zugeordnet" disabled>
                    Löschen
                  </button>
                @endif
          </div>
        </div>
      </div>
    @endforeach
  </div>
</div>

@include("matches")

<div class="container p-3 ml-0">
  <a href="{!! route("addHall") !!}" class="btn btn-primary" role="button">Neue Halle anlegen</a>
  <a href="{!! route("addMatch") !!}" class="btn btn-secondary" role="button">Neue Partie anlegen</a>
</div>

@endsection
