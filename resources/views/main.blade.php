<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <title>Baskethall application</title>
    
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <link rel="stylesheet" href="/css/footer.css">
    
  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
</head>

<body>
  <header>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary mb-3">
      <a class="navbar-brand" href="#">Baskethall</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNav">
          <ul class="navbar-nav">
            <li class="nav-item {{ Request::is('/') ? 'active' : '' }}">
              <a class="nav-link" href="{!! route('home') !!}">Home</a>
            </li>
            <li class="nav-item {{ Request::is('dateview') ? 'active' : '' }}">
              <a class="nav-link" href="{!! route('dateview') !!}">Terminansicht</a>
            </li>
          </ul>
        </div>
    </nav>
  </header>

  <main class="mb-2">
    @yield("content")
  </main>

  <footer class="footer">
    <div class="container text-center">
      <span class="text-muted">Baskethall application &copy; UB-WebTech-Gruppe 2018</span>
    </div>
  </footer>
</body>
</html>
