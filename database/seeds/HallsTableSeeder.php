<?php

use Illuminate\Database\Seeder;
use App\Hall;

class HallsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        Hall::create([
            "name" => "brose ARENA",
            "address" => "Forchheimer Straße. 15, 96025 Bamberg, Deutschland",
            "seat_count" => 10000,
            "entrance_exit_count" => 10
        ]);

        Hall::create([
            "name" => "Audi Dome",
            "address" => "Grasweg 74, 81373, München, Deutschland",
            "seat_count" => 7200,
            "entrance_exit_count" => 8
        ]);

        Hall::create([
            "name" => "Mercedes-Benz-Arena",
            "address" => "Mercedes Platz 1, 10243 Berlin-Friedrichshain",
            "seat_count" => 145000,
            "entrance_exit_count" => 15
        ]);

        Hall::create([
            "name" => "ratiopharm Arena",
            "address" => "Europastraße 25, 89231 Neu-Ulm",
            "seat_count" => 6200,
            "entrance_exit_count" => 7
        ]);

        Hall::create([
            "name" => "Volkswagen Halle",
            "address" => "Braunschweig",
            "seat_count" => 6100,
            "entrance_exit_count" => 9
        ]);

        Hall::create([
            "name" => "Telekom Dome",
            "address" => "Basketsring 1, Bonn",
            "seat_count" => 6000,
            "entrance_exit_count" => 6
        ]);

        Hall::create([
            "name" => "Fraport Arena",
            "address" => "Silostraße 46, Frankfurt am Main",
            "seat_count" => 5002,
            "entrance_exit_count" => 8
        ]);
    }
}
